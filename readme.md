# GAME TOOL BOX

## Installation
git clone https://Studiodetelevision@bitbucket.org/mediatools31/gametoolbox.git  

puis effacer le dossier .git pour laisser la place au versionning du jeu  
rm -rf .git  

npm install  

puis node ./gb/tools/init.js pour créer les dossiers par defaut et copier les fichiers de config  


## DONNEES JSON
data_schema.json le schema de données  
data_defaults.json les valeurs par defaut

le data_schema comporte des nodes par defaut :
settings
audio
graphics

(voir les data_schema et data_defaults fournis)

le settings sera restitué par le bo dans un fichier settings.json
le reste sera dans le data.json
il permet un preload rapide des paramètres de base de l'application


## WEBAPP et PWA
la box prend en charge le format webapp et le fonctionnement pwa via webpack
et génère les manifest.json, le service worker et le precache


## index.html
il est autogénéré depuis le template gb/html/index.ejs
il peut être overridé en mettant un /html/index.ejs à la racine

## CSS
injecte un css de base (voir gb/assets/css) qui gère le responsive du conteneur principal
incluant les materials icons

## CONVENTION pour settings
title
theme
cssoverride

## CONVENTION pour data
il contient au minimum les noeuds settings et audio

# LE BOX.JS contient :

## Le preloader

GB.signal.on("preload_done",function_init_de_l_app);
Le système est prévu pour preloader systématiquement le settings.json (très léger) puis le data.json (qui peut être plus lourd)
La vue est à instancier sur le conteneur "#gamerootelement"

new Vue({
  router,
  render: h => h(MainVue)
}).$mount("#gamerootelement")

## Un objet global window.GB

Le game tool box une boite à outil de développement qui inclut:
*** un sanbox de dev avec webpack
*** la logique webapp + pwa
*** un système de preloader
*** une collection de fonctions communes mis à dispo via un objet global window.GB

## GB.signal  

un gestionnaire dévènement en pur javascript
permettant une communication générale de l'app
quelle que soit la technologie
écouteurs: GB.signal.on('mon_event',ma_function)
émetteur: GB.signal.emit('mon_event',{donnees})


## GB.config
contient les infos du petit fichier de settings.json

## GB.data
contient les données du data.json

## GB.$data("data_path")
permet d'acceder aux données dans le json.
le chemin est donné sous la forme d'une chaine de caractère

## GB.audio

un petit gestionnaire audio
qui préloade les sons définis dans le "audio" du data.json
GB.audio.play("music_intro",true)
GB.audio.stop("music_intro");

## GB.template("<b>Hello !</b> {{ nom }} {{ prenom }}",{nom:"doe",prenom:"john"})

permet de proposer des variables dynamiques dans l'admin
ex: pour afficher le score en cours dans un message de félicitation etc etc ....

## GB.browser  
une instance de bowser  
https://www.npmjs.com/package/bowser  
permettant d'avoir les infos sur les devices et plateformes des clients


##ANIMATE.CSS
import de la librairie d'animation css
pour des animations
https://github.com/daneden/animate.css


##OUTIL EN LIGNE DE COMMANDE

tools/generatedefaults.json
pour le développement, génère un data.json depuis le data_schema et le data_defaults
(par commodité cet outil prend aussi en compte des valeurs "default" indiquées dans le schema)
il faut donc remplir en amont ces fichiers

gb/tools/init.json
qui va générer les dossiers de base


gb/tools/generatedefaults.json
va merger les default du data_schema et les données du data_defaults dans data_defaults

gb/tools/generatefolders.json (pour développement essentiellement)
va merger les default du data_schema et les données du data_defaults dans build/data.json
et génère aussi un settings.son
