const fs=require('fs');
const _=require("lodash")
var gamePath=__dirname+"/../../";
var schema=require(gamePath+'data_schema.json');
var baseDefaults=require(gamePath+'data_defaults.json');

var defaults={}

var parseObjects=function(c) {

var ret={};
  if (c.type=="object") {

    for (var p in c.properties) {

    ret[p]=parseObjects(c.properties[p]);

    }

  }
  else {

    if (c.default) ret=c.default
    else {
      switch (c.type) {

      case "array" , "arraymultitypes":

      ret=[];
      break;

      case "string" , "image":

      ret="";
      break;

      default:

      ret={};

      break;


      }
    }


    //if (c.defaults) ret.p=


  }


  return ret;




}

for (var e in schema) {
defaults[e]=parseObjects(schema[e]);



}



defaults=_.mergeWith(defaults,baseDefaults);


var settings=JSON.stringify(defaults.settings, null, 4);
fs.writeFileSync(gamePath+'build/settings.json', settings);

var data=JSON.stringify(defaults, null, 4);
fs.writeFileSync(gamePath+'build/data.json', data);

console.log('data.json and settings.json generated')
