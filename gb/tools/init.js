const fs = require('fs-extra')

function ensureDirSync (dirpath) {
  try {
    fs.mkdirSync(dirpath, { recursive: true })
  } catch (err) {
    if (err.code !== 'EEXIST') throw err
  }
}

var root=__dirname+"/../../";
ensureDirSync(root+"build/assets/css/themes/default");
ensureDirSync(root+"src");
ensureDirSync(root+"html");

const content = ''

try {
  const data = fs.writeFileSync(root+"build/assets/css/default/style.css", content)
  //file written successfully
} catch (err) {
  console.error(err)
}

var source = root+"gb/config";
var destination = root
fs.copy(source, destination, function (err) {
    if (err){
        console.log('An error occured while copying the folder.')
        return console.error(err)
    }
    console.log('Copy completed!')
});
var source = root+"gb/html";
var destination = root+"html"
fs.copy(source, destination, function (err) {
    if (err){
        console.log('An error occured while copying the folder.')
        return console.error(err)
    }
    console.log('Copy completed!')
});
