const fs=require('fs');
const _=require("lodash")
var gamePath=__dirname+"/../../";
var schema=require(gamePath+'data_schema.json');
var baseDefaults=require(gamePath+'data_defaults.json');

var defaults={}

var parseObjects=function(c) {

var ret={};
  if (c.type=="object") {

    for (var p in c.properties) {

    ret[p]=parseObjects(c.properties[p]);

    }

  }
  else {

    if (c.default) {



switch (c.type) {

  case  "audio":

  ret={name: "",url:c.default};
  break;

case  "image":

  ret={title: c.title,alt: c.title,url:c.default};
break;

default:

ret=c.default

break;


}


    }
    else {
      switch (c.type) {

      case "array" , "arraymultitypes":

      ret=[];
      break;

      case "string" , "image":

      ret="";
      break;

      default:

      ret={};

      break;


      }
    }


    //if (c.defaults) ret.p=


  }


  return ret;




}

for (var e in schema) {
defaults[e]=parseObjects(schema[e]);



}



defaults=_.mergeWith(defaults,baseDefaults);




var data=JSON.stringify(defaults, null, 4);
fs.writeFileSync(gamePath+'data_defaults.json', data);

console.log('data_defaults.json generated')
