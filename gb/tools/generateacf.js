const fs=require('fs');
const _=require("lodash")
var gamePath=__dirname+"/../../";
var schema=require(gamePath+'data_schema.json');
var baseDefaults=require(gamePath+'data_defaults.json');

var uniqid = function() {
return (new Date().getTime() + Math.floor((Math.random()*10000)+1)).toString(16);
};

var formatName=function(t) {
  return t.toLowerCase();
}

var FORMUID=uniqid();
var acf=[{"title":"import_"+FORMUID,
    "key": "group_"+FORMUID,
    "menu_order": 1,
    "position": "normal",
    "style": "default",
    "label_placement": "left",
    "instruction_placement": "field",
    fields:[]}]
var acfNode=acf[0];
var acfFields=acfNode.fields;
var parseTabs=function(node) {

}

// acf types
// textarea
// number
// email
// password
// wysiwyg
// oembed
// image



var parseObjects=function(key,c) {

var ret={};





switch (c.type) {
  // case  "object":
  // for (var p in c.properties) {
  //
  // parseObjects(p,c.properties[p]);
  //
  // }
  //   break;
  case  "number":

  ret = {key:"field_"+uniqid(),label:c.title,name:formatName(key),type:"number"};
  break;

case  "textarea":

ret = {key:"field_"+uniqid(),label:c.title,name:"txt_"+formatName(key),type:"textarea"};
break;

  case  "string":

  ret = {key:"field_"+uniqid(),label:c.title,name:"txt_"+formatName(key),type:"text"};
  break;
  case  "translation":

  ret = {key:"field_"+uniqid(),label:c.title,name:"txt_"+formatName(key),type:"text"};

  break;
  case  "boolean":

  ret = {key:"field_"+uniqid(),label:c.title,name:formatName(key),type:"true_false"};
  break;
  case  "file":

  ret = {key:"field_"+uniqid(),label:c.title,name:formatName(key),type:"file","return_format": "id"};
  break;
  case  "audio":

  ret = {key:"field_"+uniqid(),label:c.title,name:"son_"+formatName(key),type:"file","return_format": "id"};
  break;

case  "image":

  ret = {key:"field_"+uniqid(),label:c.title,name:"img_"+formatName(key),type:"image","return_format": "id"};
break;
case  "array":

  ret = {key:"field_"+uniqid(),label:c.title,"button_label": "Ajouter",name:formatName(key),type:"repeater","layout": "row"};
  ret.sub_fields=[];
  for (var f in c.items.properties) {
ret.sub_fields.push(parseObjects(f,c.items.properties[f]))
  }



break;
default:
  ret = {key:"field_"+uniqid(),label:c.title,name:formatName(key),type:"text"};
break;



}

return ret;






}

for (var e in schema) {
  var node=schema[e];
  var field={
    key:"field_"+uniqid(),
      "label": node.title,
      "name": "tab_"+formatName(e),
      "type": "tab",
      "instructions": node.description || ""
  }
acfFields.push(field);

// SOIT CE SONT DES OBJECT SOIT DES ARRAY

//OBJECT
if (node.type=="object") {
  for (var p in node.properties) {

    // "title": {
    //     "title": "Titre du jeu",
    //     "type": "string",
    //     "default":"Rallye Quizz"
    // },
    acfFields.push(parseObjects(p,node.properties[p]));



  }
}

// ARRAY
if (node.type=="array") {

  acfFields.push(parseObjects(e,node));
}

}







var data=JSON.stringify(acf, null, 4);
fs.writeFileSync(gamePath+'data_acf_'+FORMUID+'.json', data);

console.log('data_acf.json generated')
