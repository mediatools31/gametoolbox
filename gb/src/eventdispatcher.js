function EventDispatcher() {}




Object.assign(EventDispatcher.prototype, {

  addEventListener: function(type, listener) {

    if (this._listeners === undefined) this._listeners = {};

    var listeners = this._listeners;

    if (listeners[type] === undefined) {

      listeners[type] = [];

    }

    if (listeners[type].indexOf(listener) === -1) {

      listeners[type].push(listener);

    }

  },

  hasEventListener: function(type, listener) {

    if (this._listeners === undefined) return false;

    var listeners = this._listeners;

    return listeners[type] !== undefined && listeners[type].indexOf(listener) !== -1;

  },

  removeEventListener: function(type, listener) {

    if (this._listeners === undefined) return;

    var listeners = this._listeners;
    var listenerArray = listeners[type];

    if (listenerArray !== undefined) {

      var index = listenerArray.indexOf(listener);

      if (index !== -1) {

        listenerArray.splice(index, 1);

      }

    }

  },

  dispatchEvent: function(event, data) {

    if (this._listeners === undefined) return;

    var type = (typeof event == "string") ? event : event.type;
    var data = (data) ? data : event;

    var listeners = this._listeners;
    var listenerArray = listeners[event];

    if (listenerArray !== undefined) {

      data.target = this;

      var array = listenerArray.slice(0);

      for (var i = 0, l = array.length; i < l; i++) {

        array[i].call(this, data);

      }

    }

  }

});

Object.assign(EventDispatcher.prototype, {
  on: EventDispatcher.prototype.addEventListener,
  remove: EventDispatcher.prototype.removeEventListener,
  off: EventDispatcher.prototype.removeEventListener,
  emit: EventDispatcher.prototype.dispatchEvent,
  dispatch: EventDispatcher.prototype.dispatchEvent

});


module.exports = EventDispatcher;
