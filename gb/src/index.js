var EventDispatcher = require("./eventdispatcher.js");
var audiomanager = require("./audiomanager.js");
var dom = require("./domtools.js");
var template = require("./template.js");
import preloader from "preloader"
import MyJsonPreloader from "./preloaders/json.js"
import Bowser from "bowser";
import Splashscreen from "./splashscreen.js"
//load default sounds




var gamebox = function() {};

window.gamebox = window.GB = gamebox;



var url = window.location.href.toString();
if (window.location.href.indexOf('#') != -1) url = url.substr(0, url.indexOf('#'));

gamebox.baseUrl = window.baseUrl || url;


// CHARGEMENT DU SERVCE WORKER

var base = document.querySelector("base");
var baseUrlMotor = (base) ? base.href : "/";
console.log("service worker base url", baseUrlMotor)
// Check that service workers are registered
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register(baseUrlMotor + 'sw.js').then(registration => {
      console.log('SW registered: ', registration);
    }).catch(registrationError => {
      console.log('SW registration failed: ', registrationError);
    });
  });
}


gamebox.audiomanager = window.gamebox.audio = audiomanager;

gamebox.signal = window.Signal = new EventDispatcher();

gamebox.template = template;

gamebox.browser = Bowser.getParser(window.navigator.userAgent);

var initDataLoading = function(settings) {

  gamebox.config = settings;

  var theme = "default";
  if (settings && settings.theme) {
    theme = settings.theme;

  }
  dom.addStyleSheet("assets/css/themes/" + theme + "/style.css")

  if (settings.cssoverride) {

    dom.injectCSS(settings.cssoverride)

  }

  if (settings.baseElement && document.querySelector(settings.baseElement)) {
    var parentElement = document.querySelector(content.baseElement);

  } else if (document.querySelector('#gamerootelement')) {

    var parentElement = document.querySelector('#gamerootelement');

  } else {

    var parentElement = document.createElement('div');
    document.body.appendChild(parentElement)
  }



  var s = new Splashscreen();
  parentElement.appendChild(s.el);


  var loader2 = new MyJsonPreloader(GB.baseUrl + "/data.json");
  loader2.on('complete', function(content) {

    gamebox.data = content;
    gamebox.data.settings = settings;



  var audios = []
    for (var n in gamebox.data) {
      if (n.indexOf('son_')!=-1) {
        var node=gamebox.data[n];
        if (node && node.url) {
          audios.push({
            id: n,
            src: gamebox.data[n].url

          })
        }

      }
    }
      window.gamebox.audiomanager.registerSounds(audios)




    gamebox.$data = function(pathEnString) {
      var val = eval("gamebox.data." + pathEnString)
      return val || pathEnString;
    }





    //setTimeout(function() {
      s.el.parentElement.removeChild(s.el)
      s.destroy();

      Signal.emit("preload_done");

    //}, 1000);

  });
  var progress_bar = document.querySelector('.splashscreen_progress_bar')
  loader2.on('progress', function(progress) {

    if (progress.lengthComputable) {
      progress_bar.style.width = Math.floor(100 * progress.loaded / progress.total) + "%";
    } else {

      progress_bar.style.width = Math.floor(100 * progress.loaded / 10000000) + "%";
    }
  });


  loader2.load();



}

// if (window.gameSettings) {
  initDataLoading(window.gameSettings)
// } else {
//   var loader = new MyJsonPreloader(GB.baseUrl + '/settings.json');
//   loader.on('complete', initDataLoading);
//   loader.load();
// }

// Set the name of the hidden property and the change event for visibility
var hidden, visibilityChange;
if ("hidden" in document) {
	hidden = "hidden";
	visibilityChange = "visibilitychange";
} else if ("mozHidden" in document ) {
	hidden = "mozHidden";
	visibilityChange = "mozvisibilitychange";
} else if ("msHidden" in document ) {
	hidden = "msHidden";
	visibilityChange = "msvisibilitychange";
} else if ("webkitHidden" in document ) {
	hidden = "webkitHidden";
	visibilityChange = "webkitvisibilitychange";
}

console.log('handle visibility change with '+visibilityChange)
// If the page is hidden, pause the video;
// if the page is shown, play the video
function handleVisibilityChange() {

  console.log('visibility change event ')

  if (document[hidden]) {
    console.log('page hidden')
  		 GB.audio.mute();
  		} else {
        console.log('page entered')
      		 GB.audio.unmute();

  		}
  	  }



  document.addEventListener(visibilityChange, handleVisibilityChange, false);
