module.exports.addStyleSheet=function(href) {
  // Get HTML head element
       var head = document.getElementsByTagName('HEAD')[0];

       // Create new link Element
       var link = document.createElement('link');

       // set the attributes for link element
       link.rel = 'stylesheet';

       link.type = 'text/css';

       link.href = href;

       // Append link element to HTML head
       head.appendChild(link);
}
module.exports.injectCSS=function(styles) {
  // Get HTML head element
  var css = document.createElement('style');
         css.type = 'text/css';

         if (css.styleSheet)
             css.styleSheet.cssText = styles;
         else
             css.appendChild(document.createTextNode(styles)); 

         /* Append style to the tag name */
         document.getElementsByTagName("head")[0].appendChild(css);
}
