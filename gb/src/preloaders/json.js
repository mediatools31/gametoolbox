var EventDispatcher = require("../eventdispatcher.js");


class JsonPreloader extends EventDispatcher {

  constructor(url) {
    super();
    var that = this;
    this.url=url;
    var oReq = new XMLHttpRequest();
    this.oReq = oReq;
    oReq.open("GET", url, true);
    oReq.responseType = "text";
    oReq.addEventListener("progress", this.onProgress.bind(this), false);
    oReq.addEventListener("load", this.onComplete.bind(this), false);
    oReq.addEventListener("error", this.onError.bind(this), false);
  }
  onProgress(evt) {
    this.emit('progress', evt)
  }
  onComplete(evt) {
    var response = this.oReq.responseText;

    this.emit('complete', JSON.parse(response))
  }
  onError(evt) {
    console.error('error loading ',this.url)
    this.emit('error', evt)
  }
  load() {
    this.oReq.send();
  }

}

module.exports = JsonPreloader;
