var audioCtx = new(window.AudioContext || window.webkitAudioContext)();

class Sound {
  constructor(s) {
    this.data = s;
    this.loop = false;
    if (s.id) this.id = s.id;
    this.load();

  }
  play() {
    console.log('PLAY SOUND', this.data)
    this.source = audioCtx.createBufferSource(); // creates a sound source
    this.source.loop = this.loop;
    this.source.buffer = this.buffer; // tell the source which sound to play
    this.source.connect(audioCtx.destination); // connect the source to the context's destination (the speakers)
    this.source.start(0);
  }
  stop() {
    console.log('STOP', this.data)
    if (this.source) {
      this.source.stop();

    }
  }
  load(andplay) {
    var that = this;
    var request = new XMLHttpRequest();
    request.open('GET', this.data.src, true);
    request.responseType = 'arraybuffer';

    // Decode asynchronously
    request.onload = function() {
      audioCtx.decodeAudioData(request.response, function(buffer) {
        that.buffer = buffer;
        console.log(that.data.src, "loaded")
        if (andplay) that.play();
      }, function() {
        alert('error loading sound')
      });
    }
    request.send();
  }
}

class SoundHTML {
  constructor(s) {
    this.data = s;
    this.loop = false;
    this.audio = new Audio();

    if (s.id) this.id = s.id;
    this.load();

  }
  play() {
    console.log('PLAY', this.data)
    this.audio.currentTime = 0;
    this.audio.loop = this.loop;
    var p = this.audio.play();
    console.log(this)
  }
  stop() {
    console.log('STOP', this.data)


    this.audio.pause();

  }
  load(andplay) {
    this.audio.src = this.data.src;
  }
}

class AudioManager {

  constructor() {
    this.sounds = [];
    this.muted = false;
  }
  toggleMute() {
    var that = this;
    this.muted = !this.muted;
    this.sounds.forEach(function(s) {
      s.audio.muted = that.muted;
    })
    return this.muted;
  }
 mute() {
    var that = this;
    this.muted = true;
    this.sounds.forEach(function(s) {
      s.audio.muted = that.muted;
    })
    if (window.Signal) window.Signal.emit("audiomanager_muted");
    return this.muted;
  }
 unmute() {
    var that = this;
    this.muted = false;
    this.sounds.forEach(function(s) {
      s.audio.muted = that.muted;
    })
    if (window.Signal) window.Signal.emit("audiomanager_unmuted");
    return this.muted;
  }
  registerSounds(soundData) {
    // un tableau de données audio
    // {id, id du son
    // src, source
    // events tableau de noms d evenements associés
    // }
    var that = this;
    soundData.forEach(function(s) {
      if (that.get(s.id)) {
        var exists = that.get(s.id)
        exists.data = s;
        exists.load();
      } else {
        console.log('new sound', s)
        var son = new SoundHTML(s);
        son.audio.muted = that.muted;
        that.sounds.push(son)
        if (s.events) {
          s.events.forEach(function(evt) {

            if (window.Signal) window.Signal.on(evt, son.play.bind(son))

          })
        }
      }





    })


  }

  get(id) {

    var filtrage = this.sounds.filter(function(s) {
      return s.id == id
    })
    if (filtrage) return filtrage[0];

  }


  play(soundid, looped) {
    var s = this.get(soundid);
    if (s) {
      if (looped) s.loop = true;
      s.play();
      return s;
    } else console.error('sound not found ', soundid)
  }
  stop(soundid) {
    var s = this.get(soundid);
    if (s) s.stop();
    else console.error('sound not found ', soundid)

  }
}


module.exports = new AudioManager();
