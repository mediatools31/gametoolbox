var _=require("lodash");

const template = function  (html,data) {

  _.templateSettings.interpolate = /\{\{([\s\S]+?)\}\}/g;

var compiled = _.template(html);

return compiled(data);
}

module.exports=template;
