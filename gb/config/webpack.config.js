

const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WebpackPwaManifestPlugin = require('webpack-pwa-manifest')
const CopyPlugin = require('copy-webpack-plugin');
const workboxPlugin = require('workbox-webpack-plugin');

var path = require('path');
const fs = require("fs"); // Or `import fs from "fs";` with ESM

const gbDir="./gb/";
const gbSrcDir="./gb/src/";
const srcDir="./src/";

var entries={"box":gbSrcDir+'index.js'
// ,"box.min.css":gbDir+'assets/css/base.css'
};

if (fs.existsSync(srcDir)) entries["bundle"]=srcDir+'app.js';
else if (fs.existsSync(srcDir)) entries["bundle.[hash].js"]=srcDir+'index.js';

const buildDir=__dirname +"/build/";

// fonction qui permet les overrides pour la gametoolbox
var getBoxPath=function(path) {
    return (fs.existsSync(__dirname+"/"+path)) ? __dirname+"/"+path : gbDir+path ;
}

const conf=require(getBoxPath('buildconfig.json'));

const devserver=process.env.WEBPACK_DEV_SERVER;
console.log("devserver",devserver)
console.log('DIR',__dirname)

module.exports = {

  entry: entries,
  output: {
    path: buildDir,
    filename: '[name].[hash].js'
  },
  module:{
    rules: [

    {
            test: /\.vue$/,
             exclude: /node_modules/,
            loader: 'vue-loader'
          },
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        // flags to apply these rules, even if they are overridden (advanced option)
        loader: "babel-loader",

        options: {
            "presets": ["@babel/preset-env"],
          "plugins": ["@babel/plugin-transform-arrow-functions"]
        }
          },

        {
                test: /\.css$/,
                use: [
                  'vue-style-loader',
                  'css-loader'
                ]
              }
]
},
  plugins: [
    new CopyPlugin([
      { from: getBoxPath("assets"), to: buildDir+"box" }],
    {
        ignore: [
            '.DS_Store',
            '.gitkeep'
        ]
    }
    ),
     new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      title:conf.title,
      description:conf.description,
      devserver:devserver,
      devBaseUrl:conf.devBaseUrl,
      template:getBoxPath("html/index.ejs")
    }),
    new WebpackPwaManifestPlugin({
     name: conf.title,
     description: conf.description,
     background_color: "#ffffff",
    fingerprints: false,
     icon: {
         destination: path.join('assets', 'icons'),
         src: getBoxPath('icons/icon.png'),
         sizes: [96, 512]
     }
   }),
   new workboxPlugin.GenerateSW({
    swDest: buildDir+'sw.js',
    clientsClaim: true,
    skipWaiting: true,
    exclude:/.DSStore/gm
  })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 9000,
    // writeToDisk: true
  }
}
